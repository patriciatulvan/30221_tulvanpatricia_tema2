
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
	
	private ArrayBlockingQueue<Client> clients;
	private AtomicInteger waitingTime;
	
	public Server(){
		waitingTime= new AtomicInteger();
		waitingTime.set(0);
		clients= new ArrayBlockingQueue<Client>(30);
	}
	
	public Server(Integer maxCS){
		waitingTime=new AtomicInteger();
		waitingTime.set(0);
		clients=new ArrayBlockingQueue<Client>(maxCS);
	}
	
	public void addClientToServer(Client newClient){
		if(clients.remainingCapacity()>=1){
			clients.add(newClient);
		}
		else{
			System.out.println("Full!");
		}
	}
	
	public ArrayBlockingQueue<Client> getClients(){
		return clients;
	}

	public int getNoClients(){
		return clients.size();
	}
	
	public int getWaitingTime(){
		return waitingTime.get();
	}
	
	public void setWaitingTime(Integer time) {
		waitingTime.set(time);
	}

	
	@Override
	public void run() {
		Client c= new Client();
		int time;
		while(true){
			try {
				c= clients.peek();
				if(c!=null){
					System.out.println("->"+c.getArrivalTime()+" "+ Thread.currentThread().getName());
					Thread.sleep(c.getProcessingTime()*1000);
					SimulationFrame.log.setText(SimulationFrame.log.getText()+"Clientul "+ c.getClientNumber() + " a ajuns la momenul " + c.getArrivalTime()+ ", a fost servit de catre "+Thread.currentThread().getName()+" timp de " + c.getProcessingTime() + " secunde\nsi a parasit coada la momentul "+ c.getFinishTime()+"\n");
					clients.take();
					System.out.println("       <-"+c.getArrivalTime()+ " "+ c.getFinishTime());
					time= waitingTime.get()- c.getProcessingTime();
					waitingTime.set(time);
				}
		
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
		}
	}
	
}
